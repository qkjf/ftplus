# ftplus

https://hub.docker.com/repository/docker/runphp/ftplus

提供 git ossutil npm maven 等工具

## shell示例

```shell
#!/bin/bash

ftplus() {
    (docker run -ti --rm \
        -v $(pwd):/app/src \
        -v ${HOME}/.config:/root/.config \
        -v ${HOME}/.ssh:/root/.ssh \
        -v ${HOME}/.m2:/root/.m2 \
        runphp/ftplus "$@")
}

git() {
    (ftplus git "$@")
}

mvn() {
    (ftplus mvn "$@")
}
ossutil() {
    (ftplus ossutil "$@")
}

# 更新代码
cd yanshi-cloud
git reset --hard
git pull --rebase origin develop

# 后端代码打包
cd /root/yanshi-cloud
mvn clean package -pl yanshi-modules/yanshi-tms -am -Dmaven.test.skip=true
    
# 前端代码打包
cd  /root/yanshi-ui/yanshi-tms-pc
ftplus sh -c "export NODE_OPTIONS=--openssl-legacy-provider && npm install && npm run build:prod"

# 部署前端代码到oss
cd  /root/yanshi-ui/yanshi-tms-pc
ossutil -c /root/.config/.ossutilconfig sync /app/src/dist  oss://bucketname --delete -f
```

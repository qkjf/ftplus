FROM maven:3.8-jdk-8

LABEL maintainer="hehui"

VOLUME /app/src
WORKDIR /app/src

RUN echo '<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" \
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 \
                      https://maven.apache.org/xsd/settings-1.0.0.xsd"> \
    <localRepository>/root/.m2/repository</localRepository> \
    <mirrors> \
        <mirror> \
            <id>aliyunmaven</id> \
            <mirrorOf>*</mirrorOf> \
            <name>阿里云公共仓库</name> \
            <url>https://maven.aliyun.com/repository/public</url> \
        </mirror> \
    </mirrors> \
</settings>' > /usr/share/maven/ref/settings.xml

###########################################################################
# Node / NVM:
###########################################################################

ENV NODE_VERSION 16.16.0
ENV NPM_REGISTRY https://registry.npmmirror.com
ENV NPM_FETCH_RETRIES 2
ENV NPM_FETCH_RETRY_FACTOR 10
ENV NPM_FETCH_RETRY_MINTIMEOUT 10000
ENV NPM_FETCH_RETRY_MAXTIMEOUT 60000
ENV NVM_DIR /root/.nvm
# Install nvm (A Node Version Manager)
RUN mkdir -p $NVM_DIR && \
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash \
        && . $NVM_DIR/nvm.sh \
        && nvm install ${NODE_VERSION} \
        && nvm use ${NODE_VERSION} \
        && nvm alias ${NODE_VERSION} \
        && npm config set fetch-retries ${NPM_FETCH_RETRIES} \
        && npm config set fetch-retry-factor ${NPM_FETCH_RETRY_FACTOR} \
        && npm config set fetch-retry-mintimeout ${NPM_FETCH_RETRY_MINTIMEOUT} \
        && npm config set fetch-retry-maxtimeout ${NPM_FETCH_RETRY_MAXTIMEOUT} \
        && if [ ${NPM_REGISTRY} ]; then \
        npm config set registry ${NPM_REGISTRY} \
        ;fi \
        && ln -s `npm bin --global` /root/.node-bin

# Wouldn't execute when added to the RUN statement in the above block
# Source NVM when loading bash since ~/.profile isn't loaded on non-login shell
RUN echo "" >> ~/.bashrc && \
    echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.bashrc && \
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.bashrc


# Add NVM binaries to root's .bashrc
RUN echo "" >> ~/.bashrc && \
    echo 'export NVM_DIR="/root/.nvm"' >> ~/.bashrc && \
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.bashrc

# Add PATH for node
ENV PATH $PATH:/root/.node-bin

# Make it so the node modules can be executed with 'docker-compose exec'
# We'll create symbolic links into '/usr/local/bin'.
RUN find $NVM_DIR -type f -name node -exec ln -s {} /usr/local/bin/node \; && \
    NODE_MODS_DIR="$NVM_DIR/versions/node/$(node -v)/lib/node_modules" \
    ln -s $NODE_MODS_DIR/npm/bin/npm-cli.js /usr/local/bin/npm && \
    ln -s $NODE_MODS_DIR/npm/bin/npx-cli.js /usr/local/bin/npx

RUN if [ ${NPM_REGISTRY} ]; then \
    . ~/.bashrc && npm config set registry ${NPM_REGISTRY} \
;fi

###########################################################################
# ossutil:
###########################################################################
RUN mkdir -p $HOME/.oss/bin
RUN curl -o $HOME/.oss/bin/ossutil http://gosspublic.alicdn.com/ossutil/1.7.11/ossutil64 && \
    chmod a+x $HOME/.oss/bin/ossutil && \
    echo "" >> ~/.bashrc && \
    echo 'export PATH="$HOME/.oss/bin:$PATH"' >> ~/.bashrc
# Add PATH for ossutil
ENV PATH $PATH:/root/.oss/bin

###########################################################################
# YARN NPM PNPM:
###########################################################################
RUN npm install --location=global yarn@1.22.19
RUN npm install --location=global npm@8.14.0
RUN npm install --location=global pnpm@7.14.0